package excepciones;

import javax.swing.JOptionPane;

/**
 *
 * @author alberto
 */
public class Excepciones {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
            
        //division();
        operacion2numeros();
            
    }
    
    public static void division(){
        boolean correcto=false;
        String textonum1, textonum2;
        int num1, num2, resultado;
        
        do { 
            try {
                textonum1=JOptionPane.showInputDialog("Escriba número 1 entero");
                num1= Integer.parseInt(textonum1);
                textonum2=JOptionPane.showInputDialog("Escriba número 2 entero");
                num2= Integer.parseInt(textonum2);
                resultado = num1/num2;
                System.out.println("El resultado es : " + resultado);
                correcto=true;
        
        
            } catch (ArithmeticException e) {
                System.out.println("No se puede dividir entre 0 " + e);
            
            }
        }while (!correcto);
    }
    
    // Pedimos dos numeros al usuario y el simbolo de la operacion a realizar
    // Evitar el error que se produce cuando te pide un número e introduces otro caracter, por ejemplo, una letra
    
    public static void operacion2numeros(){
    double n1,n2;
    char c='+' ;
    boolean correcto= false;
    String texton1, texton2, textoCaracter;
    do {
        // Todos los datos que que introduce el usuario los metemos en el try para evitar los posibles errores
        try {
            // Pedimos numero 1 al usuario
        texton1 = JOptionPane.showInputDialog("Introduce numero 1");
            // Convertimos el String a char
        n1=Double.parseDouble(texton1);
            // Pedimos numero 2 al usuario
        texton2 = JOptionPane.showInputDialog("Introduce numero 2");
            // Convertimos el String a número (double)
        n2=Double.parseDouble(texton2);
            // Pedimos caracter de operacion al usuario
        textoCaracter = JOptionPane.showInputDialog("Introduce caracter de la operación");
            // Convertimos el String a char
        c=textoCaracter.charAt(0);
            // Si hemos metido los datos correctamente cambiamos el booleano a true para salir del do while
        correcto=true;
            // Switch para que haga la operacion correspondiente
            switch (c){
                case '+': System.out.println("Suma: "+(n1+n2));
                            break;
                case '-': System.out.println("Resta: "+(n1-n2));
                            break;
                case '*': System.out.println("Multiplicación: "+n1*n2);
                            break;
                case '/': if(n2!=0){
                                System.out.println("División: "+n1/n2);}
                          else{
                                System.out.println("No se puede dividir entre 0");
                            }
                            break;
                default:
                            System.out.println("Operación incorrecta");
                            break;
            }
        }
    
            // Acaba el try y empieza el catch para evitar el error que sucede al pedir un numero y meterle otro caracter 
        catch (NumberFormatException e ){
            // Mensaje que mostramos cuando ocurre el error
            System.out.println("Introduce un numero" + e);
        }
    }while (!correcto);
    }
    
}
